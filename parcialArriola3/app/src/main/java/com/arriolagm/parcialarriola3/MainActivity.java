package com.arriolagm.parcialarriola3;

import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextInputEditText tgenero;
    TextInputEditText traza;
    TextInputEditText tpeso;
    TextInputEditText tcolor;
    TextInputEditText tdescripcion;
    Button bboton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tgenero=(TextInputEditText)findViewById(R.id.genero);
        traza=(TextInputEditText)findViewById(R.id.raza);
        tpeso=(TextInputEditText)findViewById(R.id.peso);
        tcolor=(TextInputEditText)findViewById(R.id.color);
        tdescripcion=(TextInputEditText)findViewById(R.id.editText5);
        bboton=(Button)findViewById(R.id.registrar);

        bboton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Registrar(v);
            }
        });
    }
    private boolean validaGenero()
    {
        String genero = tgenero.getText().toString().trim();
        if(genero.isEmpty()){
            tgenero.setError("Ingrese datos");
            return false;
        } else{
            tgenero.setError(null);
            return true;
        }
    }

    private boolean validaRaza()
    {
        String raza = traza.getText().toString().trim();
        if(raza.isEmpty()){
            traza.setError("Ingrese datos");
            return false;
        } else{
            traza.setError(null);
            return true;
        }
    }
    private boolean validaPeso()
    {
        String peso = tpeso.getText().toString().trim();
        if(peso.isEmpty()){
            tpeso.setError("Ingrese datos");
            return false;
        } else{
            tpeso.setError(null);
            return true;
        }
    }

    private boolean validaColor()
    {
        String color = tcolor.getText().toString().trim();
        if(color.isEmpty()){
            tcolor.setError("Ingrese datos");
            return false;
        } else{
            tcolor.setError(null);
            return true;
        }
    }

    private void Registrar(View v){
        if(!validaColor()|!validaGenero()|!validaPeso()|!validaRaza()){
            return;
        }
        Gato gato  =new Gato();
        String genero= tgenero.getText().toString();
        String peso= tpeso.getText().toString();
        String raza= traza.getText().toString();
        String color= tcolor.getText().toString();
        String desc= tdescripcion.getText().toString();

        gato.setColor(color);
        gato.setGenero(genero);
        gato.setPeso(peso);
        gato.setRaza(raza);
        gato.setDescripcion(desc);


        Toast.makeText(this,"Genero: "+gato.getGenero()+"\n"+"Raza: "+gato.getRaza()+"\n"+"Peso: "+gato.getPeso()
                +"\n"+"Color: "+gato.getColor()+"\n"+"Descripcion: "+gato.getDescripcion(),Toast.LENGTH_LONG).show();
    }
}
