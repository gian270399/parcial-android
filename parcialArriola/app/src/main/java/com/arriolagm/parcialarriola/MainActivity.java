package com.arriolagm.parcialarriola;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void btnCalcularClick(View V){
        EditText primernumero = (EditText) findViewById(R.id.primernumero);
        EditText segundonumero = (EditText) findViewById(R.id.segundonumero);

        int numero1 = Integer.parseInt(primernumero.getText().toString());
        int numero2 = Integer.parseInt(segundonumero.getText().toString());

        RadioButton radioSuma = (RadioButton) findViewById(R.id.radioSuma);
        RadioButton radioResta = (RadioButton) findViewById(R.id.radioResta);
        RadioButton radioDividir = (RadioButton) findViewById(R.id.radioDividir);
        RadioButton radioMultiplicar = (RadioButton) findViewById(R.id.radioMultiplicar);

        int resultado = 0;

        if(radioSuma.isChecked()){
            resultado = numero1 + numero2;
        }else if(radioResta.isChecked()){
            resultado = numero1 - numero2;
        }else if(radioDividir.isChecked()){
            resultado = numero1 / numero2;
        }else if(radioMultiplicar.isChecked()){
            resultado = numero1 * numero2;
        }

        TextView txtresultado = (TextView) findViewById(R.id.txtresultado);
        txtresultado.setText("Resultado: " + resultado);

    }
}
